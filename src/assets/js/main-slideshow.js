import Swiper from 'swiper';

const swiper = new Swiper('.main_slideshow', {
  preloadImages: false,
  lazy: {
    loadPrevNext: true,
    loadOnTransitionStart: true,
  },
  autoplay: {
    delay: 5000,
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true
  }
});
