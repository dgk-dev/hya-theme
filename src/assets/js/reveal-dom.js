import { on } from './event';

export default function () {
  const revealElems = document.querySelectorAll('[data-reveal]');

  // hide elems
  revealElems.forEach((elem) => {
    elem.className += ' reveal reveal--hide';
  });


  const checkPosition = (position) => {
    revealElems.forEach((elem) => {
      if (position >= elem.offsetTop - window.innerHeight / 1.85 && elem.classList.contains('reveal--hide')) {
        elem.classList.remove('reveal--hide');
        elem.classList.add('reveal--show');
      }
    });
  };


  // listen for position
  on('scroll-position', checkPosition);
  checkPosition(window.pageYOffset);
}
