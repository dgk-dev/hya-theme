import Nav from './nav';
import revealDom from './reveal-dom';
import lazyImages from './lazy-images';

import './main-slideshow';
import servicesModal from './services';
// import registerSW from './sw-register';


(new Nav(
  document.querySelector('.main_nav'),
  document.querySelector('.icon-mobile')
));
  
revealDom();

lazyImages();

if(pageGlobal.isFrontPage){
  servicesModal();
}

