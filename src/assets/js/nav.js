import _throttle from 'lodash/throttle';
import { emit } from './event';


//t = current time
//b = start value
//c = change in value
//d = duration
const easeInOutQuad = function (t, b, c, d) {
  t /= d / 2;

  if (t < 1) {
    return c / 2 * t * t + b;
  }

  t -= 1;
  return -c / 2 * (t * (t - 2) - 1) + b;
};

export default class {
  constructor (navElem, mobileBtn) {
    this.navElem = navElem;
    this.mobileBtn = mobileBtn;

    if (this.navElem) {
      this.position = window.pageYOffset;
      this.linkElems = this.navElem.querySelectorAll('a[href^="#"]');

      this.animateToPosition = this.animateToPosition.bind(this);
      this.toggleMobileNav = this.toggleMobileNav.bind(this);

      this.bindEvents();
    }
  }

  bindEvents () {
    const throttleChangePos = _throttle(() => { this.changePosition(); }, 100)
    window.addEventListener('scroll', throttleChangePos, false);
    this.linkElems.forEach(elem => {
      elem.addEventListener('click', this.animateToPosition, false);
    });

    if (this.mobileBtn) {
      this.mobileBtn.addEventListener('click', this.toggleMobileNav, false);
    }
  }

  changePosition () {
    const headerHeight = this.navElem.parentElement.clientHeight;

    this.linkElems.forEach(elem => {
      const targetElem = document.querySelector(elem.getAttribute('href'));
      if (targetElem.offsetTop - headerHeight <= this.position
          && targetElem.offsetTop - headerHeight + targetElem.clientHeight > this.position
          && !elem.parentElement.classList.contains('active')
        ) {
        this.navElem.querySelector('.active').classList.remove('active');
        elem.parentElement.classList.add('active');
      }
    });
    this.position = window.pageYOffset;
    emit('scroll-position', this.position);
  }

  animateToPosition (e) {
    e.preventDefault();

    const start = this.position;
    const to = document.querySelector(e.target.getAttribute('href')).offsetTop - this.navElem.parentElement.clientHeight;
    const change = to - start;
    const increment = 20;
    let currentTime = 0;

    const animateScroll = () => {
      currentTime += increment;
      const val = easeInOutQuad(currentTime, start, change, 600);
      window.scroll(0, val);
      this.position = window.pageYOffset;

      if (currentTime < 600) {
        setTimeout(animateScroll, increment);
      }
    };

    this.toggleMobileNav();
    animateScroll();
  }

  toggleMobileNav () {
    if (this.navElem.classList.contains('open')) {
      this.navElem.classList.remove('open');
    } else {
      this.navElem.classList.add('open');
    }
  }
};
