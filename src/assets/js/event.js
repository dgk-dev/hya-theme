const CEvent = CEvent || {};

export const on = (type, cb) => {
  CEvent[type] = CEvent[type] || [];
  CEvent[type].push(cb);
};

export const emit = (type, args) => {
  CEvent[type] && CEvent[type].forEach(cb => cb(args));
};
