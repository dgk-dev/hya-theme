
export default function () {
  const servicesList = document.querySelectorAll('.services__item');
  const modalContainer = document.querySelector('#modal');
  const modalBody = modalContainer.querySelector('.modal__body');
  const closeModalBtn = modalContainer.querySelector('.modal__close');

  const prefixes = ['', 'webkit', 'ms'];

  const showModal = function (e) {
    e.preventDefault();

    const icon = this.querySelector('.services__item-icon').cloneNode(true);
    const title = this.querySelector('.services__item-title');
    const info = this.querySelector('.services__item-info');

    icon.removeChild(icon.querySelector('.icon-plus'));
    const html = `
      <div class="services__item-icon circle">${icon.innerHTML}</div>
      <h1 class="modal__title">${title.innerHTML}</h1>
      <div class="modal__info flex justify-space-around">${info.innerHTML}</div>
    `;

    modalBody.innerHTML = html;
    /*prefixes.forEach(prefix => {
      const to = prefix === '' ? 'transformOrigin' : `${prefix}TransformOrigin`;
      modalContainer.style[to] = `${this.offsetLeft}px ${this.offsetTop - this.parentElement.parentElement.offsetTop}px`;
    });*/
    modalContainer.style.display = 'block';
    modalBody.style.height = modalBody.clientHeight + 'px';

    window.setTimeout(() => {
      modalContainer.className += ' modal--open';
    }, 380);
  };

  const closeModal = () => {
    modalContainer.classList.remove('modal--open');
    modalContainer.removeAttribute('style');
    modalBody.innerHTML = '';
  };


  // bind click event
  servicesList.forEach((service) => {
    service.addEventListener('click', showModal, false);
  });
  closeModalBtn.addEventListener('click', closeModal, false);
}
