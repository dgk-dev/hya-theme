
export default function () {

  const lazyLoadImages = () => {
    let images = document.querySelectorAll('.lazy');

    const setImage = img => {
      img.src = img.dataset.src;
      img.classList.remove('lazy');
    };

    if ('IntersectionObserver' in window) {
      const imageObserver = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
          if (entry.isIntersecting) {
            const image = entry.target;
            setImage(image);
            imageObserver.unobserve(image);
          }
        });
      });

      images.forEach(image => {
        imageObserver.observe(image);
      })
    } else {
      let lazyloadThrottleTimeout;

      const lazyLoad = () => {
        if (lazyloadThrottleTimeout) {
          window.clearTimeout(lazyloadThrottleTimeout);
        }

        lazyloadThrottleTimeout = window.setTimeout(() => {
          const scrollTop = window.pageYOffset;

          images.forEach(img => {
            if (img.offsetTop < (window.innerHeight + scrollTop)) {
              setImage(img);
            }
          });

          if (images.length === 0) {
            document.removeEventListener('scroll', lazyLoad);
            window.removeEventListener('resize', lazyLoad);
            window.removeEventListener('orientationChange', lazyLoad);
          }
        }, 20);
      };

      document.addEventListener('scroll', lazyLoad, false);
      window.addEventListener('resize', lazyLoad, false);
      window.addEventListener('orientationChange', lazyLoad, false);
    }

  };

  document.addEventListener('DOMContentLoaded', lazyLoadImages);
};
