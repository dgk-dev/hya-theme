<section class="contact" id="contact" data-reveal>
    <div class="container">
        <h1 class="title">Contacto</h1>

        <div class="mt-60 flex justify-space-around ">
            <div class="contact__info">
                <h4 class="title title--noline title3">
                    Tu empresa... Nuestro compromiso.
                    Escríbenos y nos pondremos
                    en contacto contigo.
                </h4>

                <div class="contact__detail">
                    <h6 class="contact__detail-title">EMAIL</h6>
                    <p class="text">Info@hyp.mx</p>
                </div>
                <div class="contact__detail">
                    <h6 class="contact__detail-title">TELÉFONOS</h6>
                    <p class="text">
                        (452) 523 1648 <br>
                        (452) 119 5944
                    </p>
                </div>
                <div class="contact__detail">
                    <h6 class="contact__detail-title">DIRECCIÓN</h6>
                    <p class="text">Priv. De Honduras #13</p>
                    <p class="text">Col. Los Angeles</p>
                    <p class="text">C.P. 60160</p>
                </div>

                <a href="#" class="btn btn--bordered" target="_blank" rel="noreferrer noopener">
                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="30" viewBox="0 0 22 30" role="presentation" aria-hidden="true">
                        <g fill="#254061" fill-rule="nonzero">
                            <path d="M11.018 7.575a3.331 3.331 0 0 0-3.328 3.328 3.332 3.332 0 0 0 3.328 3.328 3.332 3.332 0 0 0 3.328-3.328 3.331 3.331 0 0 0-3.328-3.328zm0 4.905c-.87 0-1.577-.707-1.577-1.577s.707-1.577 1.577-1.577 1.577.707 1.577 1.577-.707 1.577-1.577 1.577z"/>
                            <path d="M11.018.005C5.01.005.121 4.894.121 10.903c0 1.326.24 2.616.7 3.83.205.5 1.037 2.347 2.792 4.14.07.064.135.136.206.198l6.933 7.365c.285.285.533 0 .533 0 1.828.05 2.909.58 3.121.848-.222.281-1.391.857-3.388.857-1.996 0-3.165-.576-3.387-.857.127-.162.576-.416 1.306-.607.003 0 .003-.002.006-.003.423-.216.19-.502.118-.577l-.012-.012-.007-.007v-.001l-.396-.402c0-.002-.001-.002-.002-.002-.509-.509-.95-.425-1.106-.372-.995.427-1.68 1.079-1.68 1.983 0 1.792 2.675 2.608 5.16 2.608 2.486 0 5.161-.816 5.161-2.608 0-1.36-1.543-2.158-3.373-2.463l5.44-5.78 1.13-1.207a14.005 14.005 0 0 0 1.533-2.367 10.773 10.773 0 0 0 1.006-4.564c0-6.01-4.889-10.898-10.897-10.898zm7.271 16.428l-6.65 7.072h-.001c-.538.573-1.01.229-1.176.07L5.065 17.84l-.061-.059a9.131 9.131 0 0 1-1.171-1.236l-.086-.113a9.056 9.056 0 0 1-1.875-5.53c0-5.044 4.103-9.148 9.146-9.148 5.043 0 9.146 4.104 9.146 9.148a9.056 9.056 0 0 1-1.875 5.53z"/>
                        </g>
                    </svg>
                    Nuestra ubicación
                </a>
            </div>
            <div class="contact__form">
                <?php echo do_shortcode('[contact-form-7 id="7" title="Contacto HYA"]'); ?>
            </div>
        </div>
    </div>
</section>
<!-- Ends contact -->