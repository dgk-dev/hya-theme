<?php get_header(); ?>
<section class="about-main">
	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
			get_template_part( 'content', 'page' );
		endwhile;
	else :
		get_template_part( 'content', 'none' );
	endif;
	?>
</section>
<!-- Ends main about -->


<?php get_footer(); ?>
