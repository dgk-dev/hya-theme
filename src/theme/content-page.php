<div class="page container container--small">
	<h1 class="title"><?php the_title(); ?></h1>
	<?php the_content(); ?>
</div>
