<?php
/**
 * Adds an HSTS header to the response.
 *
 * @param array $headers The headers to filter.
 * @return array $headers The filtered headers.
 */
add_filter( 'wp_headers', function( $headers ) {
	$headers['Access-Control-Allow-Origin'] = '*'; // Or another max-age.
	return $headers;
} );

add_filter( 'wp_service_worker_navigation_caching_strategy', function() {
	return WP_Service_Worker_Caching_Routes::STRATEGY_STALE_WHILE_REVALIDATE;
} );

add_filter( 'wp_service_worker_navigation_caching_strategy_args', function( $args ) {
	$args['cacheName'] = 'pages';
	$args['plugins']['expiration']['maxEntries'] = 50;
	return $args;
} );

wp_register_service_worker_caching_route(
	'/',
		array(
			'strategy'  => WP_Service_Worker_Caching_Routes::STRATEGY_CACHE_FIRST,
			'cacheName' => 'hya-resorces',
			'plugins'   => array(
				'expiration'        => array(
					'maxEntries'    => 60,
					'maxAgeSeconds' => 60 * 60 * 24,
			),
		),
	)
);