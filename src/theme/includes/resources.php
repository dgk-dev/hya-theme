<?php
function hya_resources() {
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    wp_enqueue_script( 'bundle_js', get_template_directory_uri() . '/js/footer-bundle.js', array(), 1.0, true );
    wp_localize_script('bundle_js', 'pageGlobal', array(
        'isFrontPage' => is_front_page()
    ));    
}

add_action( 'wp_enqueue_scripts', 'hya_resources' );

add_action( 'wp_head', 'hya_add_preload_meta', 0 );

function hya_add_preload_meta() {
    ?>
    <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/lato/S6u9w4BMUTPHh6UVSwiPGQ3q5d0.woff2" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/lato/S6u9w4BMUTPHh7USSwiPGQ3q5d0.woff2" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/lato/S6uyw4BMUTPHjx4wXiWtFCc.woff2" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/montserrat/JTURjIg1_i6t8kCHKm45_ZpC3gnD_vx3rCs.woff2" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/montserrat/JTURjIg1_i6t8kCHKm45_dJE3gnD_vx3rCs.woff2" as="font" type="font/woff" crossorigin>

    <link rel="preload" href="<?php echo get_stylesheet_uri(); ?>" as="style">

    <?php
}

function add_async_attribute($tag, $handle) {
    $scripts_to_async = array('');
     
    foreach($scripts_to_async as $async_script) {
       if ($async_script === $handle) {
          return str_replace(' src', ' async src', $tag);
       }
    }
    return $tag;
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

function add_defer_attribute($tag, $handle) {
    $scripts_to_async = array('bundle_js');
     
    foreach($scripts_to_async as $async_script) {
       if ($async_script === $handle) {
          return str_replace(' src', ' defer src', $tag);
       }
    }
    return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);


function move_jquery_into_footer( $wp_scripts ) {
    
    if( is_admin() ) {
        return;
    }
    
    $wp_scripts->add_data( 'jquery', 'group', 1 );
    $wp_scripts->add_data( 'jquery-core', 'group', 1 );
    $wp_scripts->add_data( 'jquery-migrate', 'group', 1 );
}
add_action( 'wp_default_scripts', 'move_jquery_into_footer' );