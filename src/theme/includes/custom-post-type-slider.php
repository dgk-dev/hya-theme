<?php

add_action( 'init', 'hya_custom_post_type_fpost' );

function hya_custom_post_type_fpost() {
	$labels = array(
		'name'               => _x( 'Slides', 'post type general name', 'text-domain' ),
		'singular_name'      => _x( 'Slide', 'post type singular name', 'text-domain' ),
		'menu_name'          => _x( 'Slides', 'admin menu', 'text-domain' ),
		'add_new'            => _x( 'Añadir nuevo', 'slide', 'text-domain' ),
		'add_new_item'       => __( 'Añadir nuevo slide', 'text-domain' ),
		'new_item'           => __( 'Nuevo slide', 'text-domain' ),
		'edit_item'          => __( 'Editar slide', 'text-domain' ),
		'view_item'          => __( 'Ver slide', 'text-domain' ),
		'all_items'          => __( 'Todos los slides', 'text-domain' ),
		'search_items'       => __( 'Buscar slide', 'text-domain' ),
		'not_found'          => __( 'No hay slide.', 'text-domain' ),
		'not_found_in_trash' => __( 'No hay slide en la papelera.', 'text-domain' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Descripción.', 'hya-theme' ),
		'public'             => false,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'exclude_from_search'=> true,
		'show_in_nav_menus'  => false,
		'rewrite' 			 => false,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail' ),
        'menu_icon'          => 'dashicons-format-gallery',
        'menu_position'       => 4,
	);

	register_post_type( 'slides', $args );
}


remove_filter('the_content','wpautop');

//decide when you want to apply the auto paragraph

add_filter('the_content','custom_formatting_fpost');

function custom_formatting_fpost($content){
    if(get_post_type()=='slides') //if it does not work, you may want to pass the current post object to get_post_type
        return $content;//no autop
    else
    return wpautop($content);
}