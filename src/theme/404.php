<?php get_header(); ?>

<section class="about about-main">
    <!-- container -->
	<div class="container">
		<!-- site-content -->
		<div class="site-content">
			<article class="page">
				<h1 class="page-title">404</h1>
				<br>
				<div class="page-content">
					<p>La página solicitada no se encuentra en el servidor.</p>
				</div>
			</article>
		</div>
		<!-- /site-content -->
	</div>
	<!-- /container -->
</section>
<?php get_footer(); ?>
