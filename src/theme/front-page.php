<?php get_header(); $template_uri = get_template_directory_uri(); ?>
<?php 
    $args = array(
        'post_type' => 'slides',
        'posts_per_page' => -1,
        // 'orderby' => 'ID',
        // 'order' => 'DESC',
        'post_status' => 'publish'
    );

    $slides = new WP_Query($args);
?>
<section class="main_slideshow swiper-container" id="home">
    <?php if($slides->have_posts()): ?>
        <div class="swiper-wrapper">
            <?php while($slides->have_posts()): $slides->the_post(); ?>
                <figure class="swiper-slide">
                    <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" alt="<?php echo get_the_title(); ?>" />
                    <figcaption><?php the_content(); ?></figcaption>
                </figure>
            <?php endwhile; ?>
        </div>
        <div class="swiper-pagination"></div>
    <?php else: ?>
        <p>No hay slides</p>
    <?php endif ?>
</section>
<!-- Ends main slider -->

<section class="about container flex justify-space-between" id="about" data-reveal>
    <div class="about__left">
        <h1 class="title">Nosotros</h1>

        <p class="text">En Hernández & Asociados, somos un despacho de contadores y abogados especializado en PyMEs, Personas Físicas, Morales, Agricultores y Sociedades. Contamos con más de dos décadas de experiencia en Contabilidad, Asesoría Fiscal y Asesoría Jurídica. La confianza, satisfacción y permanencia de nuestros clientes respaldan la alta calidad de nuestra atención y servicio.</p>
        <a href="<?php echo get_permalink(get_page_by_path('nosotros')); ?>" class="btn btn--default btn--small">VER MÁS</a>
    </div>

    <figure class="about__img">
        <img data-src="<?php echo $template_uri; ?>/img/img-about.jpg" alt="Nosotros" class="lazy" role="presentation" aria-hidden="true" />
    </figure>
</section>
<!-- Ends about us -->

<section class="solutions" id="solutions" data-reveal>
    <div class="container">
        <hgroup>
            <h1 class="title title--noline">Nuestras soluciones</h1>
            <h3 class="subtitle">¿En qué te podemos ayudar?</h3>
            <p class="text">¿Tienes estos problemas? Cuenta con nosotros.</p>
        </hgroup>

        <ul class="solutions__list flex justify-space-between">
            <li class="solutions__item">
                <figure class="solutions__item-img">
                    <img data-src="<?php echo $template_uri; ?>/img/img-asesoria.png" alt="" class="lazy" />
                </figure>
                <div class="solutions__item-info">
                    <h2 class="title title--noline title2">Asesoría Fiscal</h2>
                    <p class="text">Brindamos cualquier tipo de soporte que se requiera para hacer afrenta de las obligaciones determinadas por el fisco federal.</p>
                </div>
            </li>
            <li class="solutions__item">
                <figure class="solutions__item-img">
                    <img data-src="<?php echo $template_uri; ?>/img/img-proteccion.png" alt="" class="lazy" />
                </figure>
                <div class="solutions__item-info">
                    <h2 class="title title--noline title2">Protección del patrimonio de nuestros clientes</h2>
                    <p class="text">Asesorándolos respecto a la mejor opción legal para el cumplimiento de sus obligaciones impositivas.</p>
                </div>
            </li>
            <li class="solutions__item">
                <figure class="solutions__item-img">
                    <img data-src="<?php echo $template_uri; ?>/img/img-asesoria-personalizada.png" alt="" class="lazy" />
                </figure>
                <div class="solutions__item-info">
                    <h2 class="title title--noline title2">Asesoría personalizada</h2>
                    <p class="text">Conduciendo a nuestros clientes de manera eficiente en los procedimientos frente a los órganos de la administración tributaria.</p>
                </div>
            </li>
            <li class="solutions__item">
                <figure class="solutions__item-img">
                    <img data-src="<?php echo $template_uri; ?>/img/img-defendemos.png" alt="" class="lazy" />
                </figure>
                <div class="solutions__item-info">
                    <h2 class="title title--noline title2">Defendemos tus derechos</h2>
                    <p class="text">Impugnando los actos de autoridad que trascienden en su esfera jurídica.</p>
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- Ends our solutions -->

<section class="services container" id="services" data-reveal>
    <h1 class="title">Nuestros servicios</h1>

    <div class="services__list flex justify-space-between">
        <div class="services__item">
            <button class="btn" type="button">
                <div class="services__item-icon circle">
                    <svg id="icon-analisis" xmlns="http://www.w3.org/2000/svg" width="43" height="47" viewBox="0 0 43 47" role="presentation" aria-hidden="true">
                        <g fill="none" fill-rule="evenodd">
                            <path d="M17.5 6.5h8v38h-8zM13.5 12.5h4v15h-4zM25.5 12.5h4v9h-4zM26.5 22.5h14v22h-14z"/>
                            <path d="M2.5 26.5h14v18h-14z"/>
                            <path stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" d="M29.5 21.5v-10h-4v-6h-8v6h-4v16M13.5 45.5h16M21.5 1.5v4M1.5 27.5h16v18h-16z"/>
                            <path stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" d="M25.5 21.5h16v24h-16zM31.5 27.5h4M31.5 33.5h4M31.5 39.5h4M7.5 33.5h4M7.5 39.5h4"/>
                        </g>
                    </svg>

                    <div class="icon-plus circle" aria-label="ver más">
                        <svg width="14" height="14" viewBox="0 0 32 32" role="presentation" aria-hidden="true">
                            <title>plus</title>
                            <path d="M31 12h-11v-11c0-0.552-0.448-1-1-1h-6c-0.552 0-1 0.448-1 1v11h-11c-0.552 0-1 0.448-1 1v6c0 0.552 0.448 1 1 1h11v11c0 0.552 0.448 1 1 1h6c0.552 0 1-0.448 1-1v-11h11c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1z"></path>
                        </svg>
                    </div>
                </div>
                <h2 class="services__item-title">Análisis fiscal de la organización</h2>
                <div class="services__item-info">
                    <p class="text">
                        El análisis fiscal de la organización, lo que busca es que nuestra firma realice un diagnóstico exacto para crear una organización que sea idónea con la realidad de su operación. El crear una estructura organizacional funcional, manejable, adaptable, pero sobre todo óptima, que no absorba cargas fiscales innecesarias para la empresa. Esta actividad nos permitirá crear, además de una plataforma adecuada de operación, una estructura competitiva, con ahorros fiscales importantes, dada la optimización de la operación.
                    </p>
                    <p class="text">
                        La reestructura empresarial lo que busca, es encontrar el hecho imponible apto para operar en México y para que la inversión extranjera, alcance su máximo potencial en nuestro país. Así el análisis fiscal de la organización lo que buscará, es ser una guía para saber y determinar si la empresa está en el mejor rubro de operación en México, a fin de elevar su competitividad.
                    </p>
                </div>
            </button>
        </div>
        <div class="services__item">
            <button class="btn" type="button">
                <div class="services__item-icon circle">
                    <svg id="icon-autoridades" xmlns="http://www.w3.org/2000/svg" width="39" height="47" viewBox="0 0 39 47" role="presentation" aria-hidden="true">
                        <g fill="none" fill-rule="evenodd" stroke="#254061" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">
                            <path fill="#FFF" d="M1.5 1.5h36v44h-36z"/>
                            <path d="M25.5 11.5h4M25.5 19.5h4M9.5 27.5h20M9.5 35.5h20M9.5 11.5h8v8h-8z"/>
                        </g>
                    </svg>

                    <div class="icon-plus circle" aria-label="ver más">
                        <svg width="14" height="14" viewBox="0 0 32 32" role="presentation" aria-hidden="true">
                            <title>plus</title>
                            <path d="M31 12h-11v-11c0-0.552-0.448-1-1-1h-6c-0.552 0-1 0.448-1 1v11h-11c-0.552 0-1 0.448-1 1v6c0 0.552 0.448 1 1 1h11v11c0 0.552 0.448 1 1 1h6c0.552 0 1-0.448 1-1v-11h11c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1z"></path>
                        </svg>
                    </div>
                </div>
                <h2 class="services__item-title">Obtención de autoridades</h2>
                <div class="services__item-info">
                    <p class="text">
                        La obtención de permisos, certificaciones, autorizaciones o inscripción en los padrones que México ha establecido para el control de los contribuyentes, se ha vuelto no sólo una cuestión de tiempo, sino una cuestión de técnica.
                    </p>
                    <p class="text">
                        La rama fiscal y administrativa, demandan cada día que las autorizaciones se obtengan con prontitud, excelencia y que su vigencia, no ponga en riesgo a la organización, por ello, nuestra firma está capacitada para solicitar cualquier tipo de autorización en materia fiscal, administrativa o incluso de comercio exterior, que permita a la organización mantener vigente su operación sin riesgos ni contingencias permanentes. Inclusive, cancelaciones NEEC, padrones de importadores o registros de maquila, puede reactivarlos en tiempos record dada la especialidad de nuestra firma.
                    </p>
                </div>
            </button>
        </div>
        <div class="services__item">
            <button class="btn" type="button">
                <div class="services__item-icon circle">
                    <svg id="icon-analisis2" xmlns="http://www.w3.org/2000/svg" width="39" height="47" viewBox="0 0 39 47" role="presentation" aria-hidden="true">
                        <g fill="none" fill-rule="evenodd" stroke="#254061" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">
                            <path fill="#FFF" d="M1.5 1.5h36v44h-36z"/>
                            <path d="M11.5 25.5l4 4 12-12"/>
                        </g>
                    </svg>


                    <div class="icon-plus circle" aria-label="ver más">
                        <svg width="14" height="14" viewBox="0 0 32 32" role="presentation" aria-hidden="true">
                            <title>plus</title>
                            <path d="M31 12h-11v-11c0-0.552-0.448-1-1-1h-6c-0.552 0-1 0.448-1 1v11h-11c-0.552 0-1 0.448-1 1v6c0 0.552 0.448 1 1 1h11v11c0 0.552 0.448 1 1 1h6c0.552 0 1-0.448 1-1v-11h11c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1z"></path>
                        </svg>
                    </div>
                </div>
                <h2 class="services__item-title">Análisis de normas jurídicas</h2>
                <div class="services__item-info">
                    <p class="text">
                        Una de las problemáticas más comunes en México, es la constante modificación de normas jurídicas. Ya sea en el ámbito material o formal, las normas jurídicas, desde el ámbito legislativo y administrativo, muchas veces vulneran derechos humanos de los contribuyentes.
                    </p>
                    <p class="text">
                        Nuestra firma está en aptitud de realizar el análisis constitucional de cualquier precepto y calificar su constitucionalidad, a fin de que las empresas puedan evaluar si es posible reclamar la constitucionalidad del precepto y llevar el asunto a la Suprema Corte de Justicia de la Nación. Este servicio se enfoca a cualquier tipo de norma legislativa, reglamentaria o a nivel de reglas de carácter general, incluidos tratados internacionales y violaciones convencionales o constitucionales.
                    </p>
                </div>
            </button>
        </div>
        <div class="services__item">
            <button class="btn" type="button">
                <div class="services__item-icon circle">
                    <svg id="icon-liberacion" xmlns="http://www.w3.org/2000/svg" width="47" height="47" viewBox="0 0 47 47" role="presentation" aria-hidden="true">
                        <g fill="none" fill-rule="evenodd" stroke="#254061" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" transform="translate(1 1)">
                            <circle cx="22.5" cy="22.5" r="22" fill="#FFF" fill-opacity=".2"/>
                            <path d="M36.5 39.5c-.3-4.8-3-6.3-7-7.6-3-1-3.9-4.2-4.1-5.9M19.6 25.9c-.2 1.7-1.1 4.9-4.1 6-4 1.3-6.7 2.8-7 7.6"/>
                            <path d="M22.5 26.5c-4.4 0-8-3.6-8-8v-2c0-4.4 3.6-8 8-8s8 3.6 8 8v2c0 4.4-3.6 8-8 8z"/>
                        </g>
                    </svg>

                    <div class="icon-plus circle" aria-label="ver más">
                        <svg width="14" height="14" viewBox="0 0 32 32" role="presentation" aria-hidden="true">
                            <title>plus</title>
                            <path d="M31 12h-11v-11c0-0.552-0.448-1-1-1h-6c-0.552 0-1 0.448-1 1v11h-11c-0.552 0-1 0.448-1 1v6c0 0.552 0.448 1 1 1h11v11c0 0.552 0.448 1 1 1h6c0.552 0 1-0.448 1-1v-11h11c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1z"></path>
                        </svg>
                    </div>
                </div>
                <h2 class="services__item-title">Liberación de obligaciones</h2>
                <div class="services__item-info">
                    <p class="text">
                        Las contingencias y problemas fiscales son el principal riesgo en las organizaciones.
                    </p>
                    <p class="text">
                        El propósito de nuestra firma, es liberarle de cualquier obligación fiscal y representarle, a fin de que ninguna incidencia afecte el patrimonio de la organización. La liberación de obligaciones incluye créditos fiscales firmes que se perdieron en fases jurisdiccionales, o bien, que fueron consentidos, al no ejercer los medios de defensa respectivos. La liberación de obligaciones, lo que busca es que los créditos fiscales que vayan a ser determinados, los ya determinados o las exigibilidades futuras de los mismos, incluso, cuando alcancen su firmeza, no representen un riesgo para la organización que impida, paralice o neutralice su capacidad de desarrollo.
                    </p>
                </div>
            </button>
        </div>
        <div class="services__item">
            <button class="btn" type="button">
                <div class="services__item-icon circle">
                    <svg id="icon-estudio" xmlns="http://www.w3.org/2000/svg" width="39" height="47" viewBox="0 0 39 47" role="presentation" aria-hidden="true">
                        <g fill="none" fill-rule="evenodd" stroke="#254061" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">
                            <path fill="#FFF" d="M1.5 1.5h36v44h-36z"/>
                            <g transform="translate(7.5 11.5)">
                                <path d="M12 0v12h12"/>
                                <circle cx="12" cy="12" r="12"/>
                            </g>
                        </g>
                    </svg>

                    <div class="icon-plus circle" aria-label="ver más">
                        <svg width="14" height="14" viewBox="0 0 32 32" role="presentation" aria-hidden="true">
                            <title>plus</title>
                            <path d="M31 12h-11v-11c0-0.552-0.448-1-1-1h-6c-0.552 0-1 0.448-1 1v11h-11c-0.552 0-1 0.448-1 1v6c0 0.552 0.448 1 1 1h11v11c0 0.552 0.448 1 1 1h6c0.552 0 1-0.448 1-1v-11h11c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1z"></path>
                        </svg>
                    </div>
                </div>
                <h2 class="services__item-title">Estudios técnicos</h2>
                <div class="services__item-info">
                    <p class="text">
                        El estudio técnico representa uno de los servicios más detallados de nuestra firma. Lo que se busca con el mismo es descubrir la verdadera génesis de la norma.
                    </p>
                    <p class="text">
                        Nuestra firma con este servicio busca que toda organización aplique la norma desentrañando su verdadera génesis. Realizamos estudios técnicos, meticulosos y sistemáticos, con la finalidad de garantizar que la interpretación de la norma, es correcta y adecuada a los intereses de la empresa.
                    </p>
                </div>
            </button>
        </div>
        <div class="services__item">
            <button class="btn" type="button">
                <div class="services__item-icon circle">
                    <svg id="icon-solucion" xmlns="http://www.w3.org/2000/svg" width="43" height="45" viewBox="0 0 43 45" role="presentation" aria-hidden="true">
                        <g fill="none" fill-rule="evenodd" stroke="#254061" stroke-linecap="round" stroke-linejoin="round" stroke-width="3">
                            <path d="M1.5 25.5h10c5.5 0 10 4.5 10 10v8M41.5 25.5h-10c-5.5 0-10 4.5-10 10v8M21.5 37.5v-36"/>
                            <path d="M7.5 31.5l-6-6 6-6M35.5 31.5l6-6-6-6M13.5 9.5l8-8 8 8"/>
                        </g>
                    </svg>

                    <div class="icon-plus circle" aria-label="ver más">
                        <svg width="14" height="14" viewBox="0 0 32 32" role="presentation" aria-hidden="true">
                            <title>plus</title>
                            <path d="M31 12h-11v-11c0-0.552-0.448-1-1-1h-6c-0.552 0-1 0.448-1 1v11h-11c-0.552 0-1 0.448-1 1v6c0 0.552 0.448 1 1 1h11v11c0 0.552 0.448 1 1 1h6c0.552 0 1-0.448 1-1v-11h11c0.552 0 1-0.448 1-1v-6c0-0.552-0.448-1-1-1z"></path>
                        </svg>
                    </div>
                </div>
                <h2 class="services__item-title">Atención y solución de facultades de comprobación</h2>
                <div class="services__item-info">
                    <p class="text">
                        El ejercicio de las facultades de comprobación cada día alcanza un mejor límite de perfección por las autoridades fiscales.
                    </p>
                    <p class="text">
                        Nuestra firma ha preparado una serie de estrategias, recomendaciones y reglas a fin de que cualquier acto de comprobación realizado en el país o en el extranjero, por cualquier autoridad o de forma individual o concurrente, se pueda llevar a cabo con el máximo apego a derecho y sobre todo, que pueda lograr liberarse de observaciones que puedan detonar una contingencia en la empresa.
                    </p>
                </div>
            </button>
        </div>
    </div>
</section>
<!-- Ends our services -->

<?php get_template_part('partials/contact-info'); ?>

<div class="modal modal-services" id="modal">
    <div class="container">
        <button class="btn modal__close">
            <span class="modal__close-txt text">Regresar a servicios</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="24" viewBox="0 0 23 24" role="presentation" aria-hidden="true">
                <path fill="none" fill-rule="evenodd" stroke="#254061" stroke-linecap="square" stroke-width="2" d="M11.5 12L22.107 1.393 11.5 12 .893 1.393 11.5 12zm0 0L.893 22.607 11.5 12l10.607 10.607L11.5 12z"/>
            </svg>
        </button>
        <div class="modal__body"></div>
    </div>
</div>
<!-- Ends modal -->
<?php get_footer(); ?>
