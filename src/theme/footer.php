	<footer class="main_f">
        <div class="container flex justify-space-between">
            <p class="text">© <?php echo date('Y'); ?> Hernández y Asociados. Todos los derechos reservados</p>

            <div class="main_f__right">
                <a href="#" class="text">Aviso de Privacidad</a>
                <a href="#" target="_blank" rel="noreferrer noopener" aria-label="facebook">
                    <svg xmlns="http://www.w3.org/2000/svg" width="8" height="18" viewBox="0 0 8 18" role="presentation" aria-hidden="true">
                        <path fill="#A7A7A7" fill-rule="nonzero" d="M7 9H5v9H2V9H0V6h2V4c0-1.47-.292-4 3-4h3v3H6a1.026 1.026 0 0 0-1 1v2h3L7 9z"/>
                    </svg>
                </a>
                <a href="#" target="_blank" rel="noreferrer noopener" aria-label="twitter">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16"  role="presentation" aria-hidden="true">
                        <path fill="#A7A7A7" fill-rule="nonzero" d="M17.953 3.98A11.5 11.5 0 0 1 6.289 16 11.793 11.793 0 0 1 0 14.19c.319.03.647.05.978.05a8.344 8.344 0 0 0 5.1-1.73A4.1 4.1 0 0 1 2.243 9.7c.254.052.512.079.771.08.365 0 .729-.047 1.082-.14A4.065 4.065 0 0 1 .803 5.63c.572.31 1.208.481 1.858.5A4.013 4.013 0 0 1 .837 2.77 3.963 3.963 0 0 1 1.391.74a11.734 11.734 0 0 0 8.457 4.22 4.073 4.073 0 0 1 4-4.96 4.139 4.139 0 0 1 2.994 1.27A8.11 8.11 0 0 0 19.45.29a4.065 4.065 0 0 1-1.8 2.24 8.428 8.428 0 0 0 2.356-.64 8.234 8.234 0 0 1-2.053 2.09z"/>
                    </svg>
                </a>
            </div>
        </div>
    </footer>
    <!-- Ends footer -->

    <?php wp_footer(); ?>
</body>
</html>
