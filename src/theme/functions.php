<?php
show_admin_bar( false );

// INCLUDES
//resources: CSS JS FONTS
require_once( trailingslashit( get_template_directory() ). '/includes/resources.php' );

//cleanup wordpress head
require_once( trailingslashit( get_template_directory() ). 'includes/cleanup.php' );

//slider post type
require_once( trailingslashit( get_template_directory() ). 'includes/custom-post-type-slider.php' );

//service worker
if(function_exists('wp_register_service_worker_caching_route')){
    require_once( trailingslashit( get_template_directory() ). 'includes/service-worker.php' );
}

//Crear páginas necesarias al activar el theme
add_action("after_switch_theme", "hya_create_pages");
add_theme_support( 'post-thumbnails' ); 

function hya_create_pages(){
    if (isset($_GET['activated']) && is_admin()){
        $pages = array(
            array(
                'title' => 'Home',
                'slug' => 'home'
            ),
            array(
                'title' => 'Nosotros',
                'slug' => 'nosotros'
            ),
        );

        foreach($pages as $page){
            $page_check = get_page_by_path($page['slug']);
            
            if(!isset($page_check->ID)){
                $page_title = $page['title'];
                $page_slug = $page['slug'];
                $page_content = '';
                $page_template = '';
                
                $new_page = array(
                    'post_type' => 'page',
                    'post_title' => $page_title,
                    'post_name' => $page_slug,
                    'post_content' => $page_content,
                    'post_status' => 'publish',
                    'post_author' => 1,
                );

                $new_page_id = wp_insert_post($new_page);
                
                if(!empty($page_template)){
                    update_post_meta($new_page_id, '_wp_page_template', $page_template);
                }
            }
        }

        $homepage = get_page_by_path('home');

        if (isset($homepage->ID)){
            update_option( 'page_on_front', $homepage->ID );
            update_option( 'show_on_front', 'page' );
        }
    }
}

add_action( 'admin_menu', 'hya_rename_labels' );

function hya_rename_labels() {
    global $menu;
    $searchPlugin = "contact-form-listing";
    $replaceName = "Contactos de formulario";
    $replaceIcon = "dashicons-email";

    $menuItem = "";
    foreach($menu as $key => $item){
        if ( $item[2] === $searchPlugin ){
            $menuItem = $key;
        }
    }
    if($menuItem){
        $menu[$menuItem][0] = $replaceName;
        $menu[$menuItem][6] = $replaceIcon;
    }
}



add_action('wp_dashboard_setup', 'hya_dashboard_widgets');

function hya_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Bienvenido', 'custom_dashboard_message');
}
 
function custom_dashboard_message() {
    echo '<div style="padding:20px 0; text-align:center;"><img src="'.get_template_directory_uri().'/img/logo.jpg"></div><p><strong>Este es el administrador del sitio Hernandez y Asociados</strong></p><p>Desde aquí podrás gestionar los slides del banner principal así como consultar y exportar la información de los usuarios que llenaron el formulario de contacto<p><p>En la sección <strong>Perfil</strong> podrás editar tus datos principales y tu contraseña.';
}

function hya_login_logo() { ?>
    <style type="text/css">
        body.login{
            background: #ffffff;
        }
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/img/logo.jpg);
            height:108px;
            width:312px;
            background-size: 312px 108px;
            background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'hya_login_logo' );

add_filter( 'login_headerurl', 'hya_loginlogo_url');

function hya_loginlogo_url($url) {
    return home_url();
}