<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hernández & Asociados | Contadores y Abogados Tributarios</title>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon-16x16.png">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/icons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-151568576-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-151568576-1');
    </script>

    <?php wp_head(); ?>
</head>
<body>
    
    <header class="main_h">
        <div class="container flex justify-space-between align-center">
            <figure class="logo">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.jpg" alt="Hernández & Asociados - Contadores y Abogados Tributarios" />
            </figure>

            <nav class="main_nav">
                <ul class="main_nav__list">
                    <li class="main_nav__item <?php echo is_front_page() ? 'active' : ''; ?>"><a href="<?php echo is_front_page() ? '' : get_home_url().'/'; ?>#home">HOME</a></li>
                    <li class="main_nav__item"><a href="<?php echo is_front_page() ? '' : get_home_url().'/'; ?>#about">NOSOTROS</a></li>
                    <li class="main_nav__item"><a href="<?php echo is_front_page() ? '' : get_home_url().'/'; ?>#solutions">NUESTRAS SOLUCIONES</a></li>
                    <li class="main_nav__item"><a href="<?php echo is_front_page() ? '' : get_home_url().'/'; ?>#services">SERVICIOS</a></li>
                    <li class="main_nav__item"><a href="<?php echo is_front_page() ? '' : get_home_url().'/'; ?>#contact">CONTACTO</a></li>
                </ul>
            </nav>

            <button class="icon-mobile"></button>
        </div>
    </header>
    <!-- Ends main header -->